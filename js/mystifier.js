/*jshint esversion: 6 */

function saveLastEditedFile(leFn) {
    localStorage.setItem("opts.lastEdited", leFn);
}

function loadLastEditedFile() {
    return localStorage.getItem("opts.lastEdited");
}

function onSaveFile(saveFinishedCallback) {
    if (_currentFileName) {
        //Save file that is already selected
        localStorage.setItem("file." + _currentFileName, jsEditor.getValue());
        saveLastEditedFile(_currentFileName);
        _isUnsaved = false;
        updateStatusBar();
        if (_.isFunction(saveFinishedCallback)) {
            saveFinishedCallback();
        }
    }
    else {
        showModalInput("Save File", "Please enter a file name", function (fn) {
            if (fn) {
                //Save the file to localStorage
                _currentFileName = fn;
                //Call the function again; this time it should save to existing file.
                if (saveFinishedCallback) {
                    onSaveFile(saveFinishedCallback);
                }
                else {
                    onSaveFile();
                }
            }
        });
    }
}

function onSaveFileAs() {
    let previousFile = _currentFileName;
    _currentFileName = null;
    onSaveFile(function () {
        if (!_currentFileName) {
            _currentFileName = previousFile;
        }
    });
}

function loadCurrentFileIntoEditor() {
    if (_currentFileName) {
        jsEditor.setValue(localStorage.getItem("file." + _currentFileName) || "");
    }
}

function getLocalStorageKeys() {
    let keys = [];
    for (var i = 0, len = localStorage.length; i < len; ++i) {
        keys.push(localStorage.key(i));
    }
    return keys;
}

function showFileBrowser(title, text, selectedFileCallback) {
    let modalContent = text;
    let filePickerContent;
    let localStorageKeys = getLocalStorageKeys();
    let savedFilesHtml = "<p><ul>";
    let savedFiles = Enumerable.from(localStorageKeys)
        .where(key => key.startsWith("file."))
        .toArray();
    for (let savedFileKeyNum in savedFiles) {
        let savedFileKey = savedFiles[savedFileKeyNum];
        let selectFileKey = "selectfile-" + htmlEncode(savedFileKey);
        savedFilesHtml += "<li><a id=\"{1}\" class=\"c-file-list-item\">{0}</a></li>".format(savedFileKey.replace("file.", ""), selectFileKey);
    }
    savedFilesHtml += "</ul></p>";
    modalContent += savedFilesHtml;
    var popup = showModalMessage(title, modalContent);
    var fileChoiceItem = $("a.c-file-list-item");
    fileChoiceItem.click(function () {
        popup.modal('hide');
        selectedFileCallback($(this).text());
    });
}

function onOpenFile() {
    showFileBrowser("Open File", "Please select a file", function (fn) {
        if (fn) {
            //Save the file to localStorage
            _currentFileName = fn;
            //Load the content into the editor
            loadCurrentFileIntoEditor();
            _isUnsaved = false;
            saveLastEditedFile(_currentFileName);
            updateStatusBar();
        }
    });
}

function onCreateNew() {
    _currentFileName = null;
    jsEditor.setValue(""); //Clear editor
    _isUnsaved = true;
    updateStatusBar();
}


//Event handlers

shortcut.add("Ctrl+S", onSaveFile);
$("#mnSave").click(onSaveFile);

shortcut.add("Ctrl+Alt+S", onSaveFileAs);
$("#mnSaveAs").click(onSaveFileAs);

shortcut.add("Ctrl+O", onOpenFile);
$("#mnLoad").click(onOpenFile);

shortcut.add("Alt+N", onCreateNew);
$("#mnNew").click(onCreateNew);


$("#mnDownload").click(function () {
    if (_currentFileName) {
        //Download current source
        let currentSketchBlob = new Blob([jsEditor.getValue()], { type: "application/javascript;charset=utf-8" });
        let blobFn = _currentFileName;
        if (!blobFn.endsWith(".pde")) {
            blobFn += ".pde";
        }
        saveAs(currentSketchBlob, blobFn);
    }
    else {
        showModalMessage("Download File", "You must save your current sketch before attempting to download it.");
    }
});

$("#mnUpload").click(function () {
    if (!window.FileReader) {
        showModalMessage("Error", "Your browser does not support this feature.");
        return false;
    }
    else {
        $("#uploadhelper").one("change", function (evt) {
            var files = evt.target.files,
                reader = new FileReader();
            reader.onload = function () {
                var uploadedSrc = this.result;
                onCreateNew();
                jsEditor.setValue(uploadedSrc);
            };
            reader.readAsText(files[0]);
        })
            .click();

    }
});

let qsSrc = urlParams.ldsrc;
if (qsSrc) {
    //Load file from query string
    jsEditor.setValue(atob(qsSrc));
}
else {
    //Reload existing file
    let leFile = loadLastEditedFile();
    if (leFile) {
        _currentFileName = leFile;
        loadCurrentFileIntoEditor();
        updateStatusBar();
    }
}