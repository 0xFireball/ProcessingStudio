/*jshint esversion: 6 */
//let msRealm = continuum.createRealm();

$(document).ready(function () {
    $('body').addClass('loaded');
});

let _isUnsaved = false;
let _currentFileName;

let cmOptions = {
    mode: "text/x-java",
    matchBrackets: true,
    theme: "solarized dark",
    lineNumbers: true,
    scrollbarStyle: "simple",
    extraKeys: { "Alt-F": "findPersistent" }
};

let jsEditor;

jsEditor = CodeMirror.fromTextArea(document.getElementById("editor"), cmOptions);
jsEditor.setSize(null, "100%");

jsEditor.on("change", function (cm, changeObj) {
    _isUnsaved = true;
    updateStatusBar();
    rerunSketch();
});

function executeSource() {
    localStorage.setItem("_.execSource", jsEditor.getValue());
    var win = window.open('execute.html', '_blank');
    if (win) {
        //Browser has allowed it to be opened
        win.focus();
    } else {
        //Broswer has blocked it
        showModalMessage("New window blocked", "Your browser blocked the request to open a new tab to run the code. Please set it to allow popups from this site and try again.");
    }
}

new Clipboard('#btnCopy', {
    text: function (trigger) {
        return outputEditor.getValue();
    }
});


function ignoreEnter(event) {
    if (event.keyCode == 13) {
        return false;
    }
}

function updateStatusBar() {
    let statusBar = $("#statusBar");
    let statusBarText;
    if (_currentFileName) {
        statusBarText = (_isUnsaved ? (_currentFileName + "*") : _currentFileName);
    }
    else {
        statusBarText = "[Unsaved]" + (_isUnsaved ? "*" : "");
    }
    statusBar.html(statusBarText);
}

let currentSketch;
function rerunSketch() {
    var pjs_source = jsEditor.getValue();
    var canvas = $("#pjs-canvas");
    var canvasDom = canvas[0];
    canvasDom.getContext('2d').clearRect(0, 0, canvasDom.width, canvasDom.height);
    try {
        var dimensions = pjs_source.match(/\s+size\((\d+),(\d+)\)/);
        let width, height;
        if (dimensions !== null) {
            width = parseInt(dimensions[1], 10);
            height = parseInt(dimensions[2], 10);
        } else {
            width = 400;
            height = 400;
        }
        canvas.css('width', width);
        canvas.css('height', height);
        if (currentSketch) {
            currentSketch.exit();
        }
        currentSketch = new Processing(canvasDom, pjs_source);
    }
    catch (err) {
        error = err;
        console.log(err);
        Processing.logger.log(err);
        if (currentSketch) {
            currentSketch.exit();
        }
    }
}

function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}


function showShareInfo() {
    let b64src = btoa(jsEditor.getValue());
    let shareInfoModal = $("#shareInfoModal");
    let shareUrlCnt = $("#shareUrl").val();
    $("#shareUrl").val(shareUrlCnt.format("{0}?ldsrc={1}".format(window.location.href.split("#")[0], b64src))); //Editor page
    let execPath = window.location.href.split("#")[0].substr(0, document.URL.lastIndexOf('/') + 1) + 'execute.html';
    $("#execShareUrl").val(shareUrlCnt.format("{0}?ldsrc={1}".format(execPath, b64src))); //Execute page
    shareInfoModal.modal("show");
}

$("#btnShare").click(showShareInfo);

$("#btnReset").click(rerunSketch);

shortcut.add("F5", function () {
    $("#mnExecute").click();
});

$("#mnExecute").click(function () {
    executeSource();
});

updateStatusBar();