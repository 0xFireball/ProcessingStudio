/*jshint esversion: 6 */
$(document).ready(function () {
    $('body').addClass('loaded');
    //Run source

    let pjs_source = localStorage.getItem("_.execSource");
    var canvas = $("#pjs-canvas")[0]; //Get DOM object of canvas

    //Check query string
    let qsSrc = urlParams.ldsrc;
    if (qsSrc) {
        //Load file from query string
        pjs_source = atob(qsSrc);
    }
    else {
        //From localstorage
        pjs_source = localStorage.getItem("_.execSource");
    }
    var p = new Processing(canvas, pjs_source);
});

$("#btnClose").click(function () {
    window.close();
});