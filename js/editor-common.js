/*jshint esversion: 6 */

var urlParams = (function (a) {
    if (a === "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i) {
        var p = a[i].split('=', 2);
        if (p.length == 1)
            b[p[0]] = "";
        else
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
})(window.location.search.substr(1).split('&'));

function showModalMessage(title, text) {
    let headerCnt = $("#modal-header-content");
    let bodyCnt = $("#modal-body-content");
    headerCnt.html(title);
    bodyCnt.html(text);
    let popup = $('#popupModal');
    popup.on('hidden.bs.modal', function () {
        headerCnt.empty();
        bodyCnt.empty();
    });
    popup.modal('show');
    return popup;
}

function showModalInput(title, text, inputCallback) {
    let headerCnt = $("#modal-header-content");
    let bodyCnt = $("#modal-body-content");
    headerCnt.html(title);
    bodyCnt.html(text);
    bodyCnt.append('<form onsubmit="return false"><input class="form-control" id="modalInput" type="text" /></form>');
    let popup = $('#popupModal');
    popup.on('hidden.bs.modal', function () {
        var inputtedText = $("#modalInput").val();
        headerCnt.empty();
        bodyCnt.empty();
        inputCallback(inputtedText);
    });
    var inputtedText = $("#modalInput").focus();
    popup.modal('show');
    return popup;
}


String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined' ? args[number] : match;
    });
};
