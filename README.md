# ProcessingStudio
A sleek, lightweight, open-source web-based editor for Processing, a simple and powerful graphics framework.

[Here](https://apps.zetaphase.io/processingstudio/) is a live preview of the application. It is written completely in JavaScript and HTML5, so it can easily be run locally or self-hosted. Simply clone the repository to your own server.

Features:
- Advanced web editor with syntax highlighting powered by CodeMirror
- Instant code preview; the canvas updates as you write the sketch
- Source and rendered application sharing (this is implemented through base64 in the URL)

The Processing canvas is powered by `processing.js`.

Licensed under the GPLv3.  
(c) ZetaPhase Technologies, 2016.
